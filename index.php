<body>
	<?php include("header.php") ?>
	<div class="main">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-md-3">
					<div class="left-img">
						<div class="col-md-12 mb-2">
							<a href="women.php">
								<img src="image/left.jpg" alt="">
							</a>
						</div>
						<div class="col-md-12">
							<img src="image/right1.jpg"  class="img-fluid hober" alt="">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="center-img">
						<div class="col-md-12 mb-2">
							<a href="">
								<img src="image/center.jpg" class="img-fluid hober" alt="">
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="right-img">
						<div class="col-md-12 mb-2">
							<a href="men.php">
							<img src="image/right.jpg"  class="img-fluid hober" alt="">
							</a>
						</div>
						<div class="col-md-12">
							<a href="">
							<img src="image/right1.jpg"    class="img-fluid hober" alt="">
						    </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="m-t-20"></div>
	<!-- 	<div class="complain">
			<marquee behavior="slide" direction="left" loop="">If you have any complain contact with unistag.com .+880267262212, or email us info@unistag.com</marquee>
	</div> -->
	<div class="container">
		<div class="week-product">
			<h2 class="item-name">This Week Specials</h2>
			<section class="customer-logos slider">
				<div class="slide">
				<a href="">
					<img src="image/e1.jpg" class="img-fluid">
				</a>								
				</div>
				<div class="slide">
					<a href=""><img src="image/e1.jpg" class="img-fluid"></a>			
				</div>
				<div class="slide">
					<a href=""><img src="image/e1.jpg" class="img-fluid"></a>
				</div>
				<div class="slide">
					<a href=""><img src="image/e1.jpg" class="img-fluid"></a>
				</div>
				<div class="slide">
					<a href=""><img src="image/e1.jpg" class="img-fluid"></a>
				</div>
			</section>
		</div>
	</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<img src="image/ad.jpg" class="img-fluid" alt="">
		</div>
	</div>
</div>
	<div class="container">
		<div class="home-product">
			<div class="row">
				<div class="col-md-3">
					<a href=""><img src="image/img1.jpg" class="img-fluid hober"
					alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/img1.jpg" class="img-fluid hober"
					alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/combo1.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-3">
					<a href="">
						<img src="image/img1.jpg" class="img-fluid hober" alt="">
					</a>
				</div>
			</div>
		</div>
		<div class="home-product">
			<div class="row">
				<div class="col-md-3">
					<a href=""><img src="image/t-shirt1.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/ladies.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-6">
					<a href=""><img src="image/tv.jpg" class="img-fluid hober" alt=""></a>
				</div>
			</div>
		</div>
		<div class="home-product">
			<div class="row">
				<div class="col-md-3">
					<a href=""><img src="image/t-shirt1.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/fan.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/mobile1.jpg" class="img-fluid hober" alt=""></a>
				</div>
				<div class="col-md-3">
					<a href=""><img src="image/ac.jpg" class="img-fluid hober" alt=""></a>
				</div>
			</div>
		</div>
	</div>
<div class="container">
<div class="col-md-12">
<img src="image/ad1.jpg" alt="">	
</div>		
</div>
	<div class="container">
		<h2 class="item-name">Men</h2>
		<div class="home-item">
			<div class="row">
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>	
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>											
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>								
			</div>			
		</div>
	</div>
	<div class="container">
		<h2 class="item-name">Men</h2>
		<div class="home-item">
			<div class="row">
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>	
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>											
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>								
			</div>			
		</div>
	</div>
	<div class="container">
		<h2 class="item-name">Women</h2>
		<div class="home-item">
			<div class="row">
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>	
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>											
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>								
			</div>			
		</div>
	</div>
	<div class="container">
		<h2 class="item-name">Kids</h2>
		<div class="home-item">
			<div class="row">
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>	
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>											
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>								
			</div>			
		</div>
	</div>
	<div class="container">
		<h2 class="item-name">Electronics</h2>
		<div class="home-item">
			<div class="row">
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>	
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>											
			<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card ">
						<div class="product">
							<img src="image/t-shirt1.jpg" class="image">
							<div class="overlay">
								<div class="text">
									<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
								</div>
								<div class="overlay-other">
									<div class="overlay-wishlist">
										<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
											<i class="fa fa-heart-o" aria-hidden="true">
											</i>
										</a>
									</div>
									<div class="overlay-view">
										<a href="" title="Quick View" class="btn btn-sm btn-warning">
											<i class="fa fa-eye" aria-hidden="true"></i>
										</a>
									</div>
									<div class="overlay-compare">
										<a href="" title="Compare" class="btn btn-sm btn-warning">
											<i class="fa fa-exchange" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<a href="" class="nav-link">
							<h5 class="card-title">Mens T-shirt</h5></a>
							<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
							<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
						</div>
					</div>
				</div>								
			</div>			
		</div>
	</div>				
	<?php include("footer.php") ?>
</body>