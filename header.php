<?php include('bootstrap.php') ?>
<body>
  <div class="header-top">
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <span><small>Hotline:&nbsp;+88016751281</small></span>
    </div>
    <div class="col-md-5 offset-md-3">
      <a href="" class="h-item">
        <span>How to use</span>
      </a>
      <a href="" class="h-item">
        <span>Cash On Delivery</span>
      </a>
      <a href="" class="h-item">
        <span>Replacement Policy</span>
      </a>
      <a href="" class="h-item">
        <span>Replacement Policy</span>
      </a>
    </div>
  </div>
</div>
  </div>
  <header>
    <div class="main-header">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <a class="navbar-brand" href="index.php">
              <img src="image/logo.png" id="logo" alt="">
            </a>
          </div>
          <div class="col-md-6 offset-md-1">
            
            <div class="input-group input-group-md p-2">
              <input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
              <div class="input-group-append">
                <span class="input-group-text" id="inputGroup-sizing-lg">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="right">
              <a href="cart.php" class="btn btn-cart" id="cart">
                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
              <span><strong>(1)</strong></span>
              </a>
              <a href="login.php" class="btn btn-signin">Sign In</a>
              <a href="register.php" class="btn btn-register">Register</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="primary">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item mainmenu0">
              <a class="nav-link " href="#">All Categories&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
              </a>
              <ul class="submenu">
                <li class="level1">
                  <a href="men.php" class="nav-link"><span>Men</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Electronics</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Kids</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item mainmenu0">
              <a class="nav-link " href="#">Women&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="submenu">
                <li class="level1">
                  <a href="" class="nav-link"><span>Accesories</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item mainmenu0">
              <a class="nav-link " href="#">Men&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="submenu">
                <li class="level1">
                  <a href="" class="nav-link"><span>Accesories</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item mainmenu0">
              <a class="nav-link " href="#">Electronics&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="submenu">
                <li class="level1">
                  <a href="" class="nav-link"><span>Accesories</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
                <li class="level1">
                  <a href="" class="nav-link"><span>Womens</span></a>
                  <ul class="submenu1">
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                    <li class="sublevel1"><a href="" class="nav-link">Clothing</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </header>
</body>