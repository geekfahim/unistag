<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <?php include('bootstrap.php') ?>
  </head>
  <body>
    <button onclick="topFunction()" id="myBtn" class="animated infinite bounce" title="Go to top"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
    <div class="m-t-100"></div>
    <div class="footer-top">
      <div class="container">
<div class="row">
<div class="col-md-6">
  <div class="input-group newsletter">
    <span class="news">Newsletter</span>
  <input type="text" class="form-control" placeholder="Enter Your Email">
  <div class="input-group-append">
    <span class="input-group-text">
      <a href=""><i class="fa fa-paper-plane" aria-hidden="true"></i></a>
    </span>
  </div>
</div>
</div>
<div class="col-md-6">
          <div class="social-icons">
          <i class="icon fa fa-twitter"></i>
          <i class="icon fa fa-facebook"></i>
          <i class="icon fa fa-google-plus"></i>
          <i class="icon fa fa-instagram"></i>
          <i class="icon fa fa-pinterest"></i>
          <i class="icon fa fa-youtube-play"></i>
        </div>
</div>
</div>        
      </div>
    </div>
    
    <footer>
      <div class="footer" id="footer">
        <div class="container">
          <div class="row">
            
            <div class="col-lg-3 col-xs-12">
              <span class="logo">
                <img src="image/logo.png" class="img-fluid" alt="logo">
              </span>
            </div>
            <div class="col-lg-3 col-xs-12">
              <h3>MENU</h3>
              <ul>
                <li><a href="#">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="service.php">Services</a></li>
                <li><a href="career.php">Career</a></li>
                <li><a href="contact.php">Contact</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-xs-12">
              <h3>CONTACT</h3>
              <ul>
                <li> <a href="#">Address</a> </li>
                <li> <a href="#">Phone</a> </li>
                <li> <a href="#">Fax</a> </li>
                <li> <a href="#">E-Mail</a> </li>
              </ul>
            </div>
            
            <div class="col-lg-3 col-xs-12">
              <h3>Map</h3>
              <img src="image/map.png" class="img-fluid" alt="">
            </div>
          </div>
        </div>
      </div>
    </footer>
    
    <div class="footer-bottom ">
      <p class="text-center">Copyright © 2018. All Right Reserved <a href="www.dreamploy.com" class="f-white">Dreamploy</a></p>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </body>
</html>