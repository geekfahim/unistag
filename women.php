<body>
	<?php include("header.php") ?>
	<div class="main-men">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="sidebar">
						<h2 class="category">Women</h2>
						<ul class="navbar-nav">
							<li class="nav-item">
								<a href="" class="nav-link">T-shirts</a>
							</li>
							<li class="nav-item">
								<a href="" class="nav-link">T-shirts</a>
							</li>
							<li class="nav-item">
								<a href="" class="nav-link">Jeans</a>
							</li>
							<li class="nav-item">
								<a href="" class="nav-link">Jeans</a>
							</li>
							<li class="nav-item">
								<a href="" class="nav-link">Jeans</a>
							</li>
							<li class="nav-item">
								<a href="" class="nav-link">Jeans</a>
							</li>														
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="home-item">
			<div class="row">
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>								
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>		
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card ">
								<div class="product">
									<img src="image/t-shirt1.jpg" class="image">
									<div class="overlay">
										<div class="text">
											<a href="product-details.php" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;Buy </a>
										</div>
										<div class="overlay-other">
											<div class="myoverlay-wishlist">
												<a href="" title="Add to Wishlist" class="btn btn-sm btn-warning">
													<i class="fa fa-heart-o" aria-hidden="true">
													</i>
												</a>
											</div>
											<div class="myoverlay-view">
												<a href="" title="Quick View" class="btn btn-sm btn-warning">
													<i class="fa fa-eye" aria-hidden="true"></i>
												</a>
											</div>
											<div class="myoverlay-compare">
												<a href="" title="Compare" class="btn btn-sm btn-warning">
													<i class="fa fa-exchange" aria-hidden="true"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<a href="" class="nav-link">
									<h5 class="card-title">Mens T-shirt</h5></a>
									<h5 class="product-price">Price:&nbsp;<span>৳</span> 650</h5>
									<h5 class="product-discount">DP:&nbsp;৳ 50</h5>
								</div>
							</div>
						</div>
					</div>						
					</div>
					<!-- row end -->
				</div>
			</div>
		</div>
	</div>
	<?php include("footer.php") ?>
</body>