      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item bg-red">
              <a class="nav-link active" href="adhome.php">
                <span class="icon"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" target="_blank" href="../index.php">
                <span class="icon"><i class="fa fa-home" aria-hidden="true"></i></span>
                Visit Site 
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="allproduct.php">
                <span class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                Product Buy
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="adorder.php">
                <span class="icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></span>
                Ordered
              </a>
            </li>
          </ul>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="adsetting.php">
                <span><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></span>
                Settings
              </a>
            </li>
          </ul>
        </div>
      </nav>