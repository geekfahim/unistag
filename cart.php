
<body>
	<?php include('header.php') ?>
	<div class="product-main">
		<div class="product-head">
			<div class="container">
				<div class="line">
				<div class="row">
					<div class="col-md-2">
						<h5>Product</h5>
					</div>
					<div class="col-md-2">
						<h5>Product Name</h5>
					</div>
					<div class="col-md-2">
						<h5>QTY</h5>
					</div>
					<div class="col-md-2">
						<h5>Price</h5>
					</div>
					<div class="col-md-1">
						<h5>DP</h5>
					</div>
					<div class="col-md-2">
						<h5>Total</h5>
					</div>
				</div>
			 </div>	
			</div>
		</div>
		<!-- product add here -->
		<div class="cart-product">
			<div class="container">
				<div class="product-here">
				<div class="row">
					<div class="col-md-2">
						<img src="image/t-shirt.jpg" class="img-thumbnail" alt="">
					</div>
					<div class="col-md-2">
						<div class="product-name">
							<p>Sleveless T-shirts</p>
						</div>
						<div class="rating">
							<span class="full"></span>
							<span class="full"></span>
							<span class="full"></span>
							<span class="half"></span>
							<span class="empty"></span>
						</div>
						<div class="product-description">
							<p><strong>Color:</strong>Blue</p>
							<p><strong>Size:</strong>
							<span class="btn">L</span>
						    </p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="qty">
						<input type="number" class="qty-num" min="1"
						max="" value="1">
						</div>
					</div>
					<div class="col-md-2">
						<p>12,000</p>
					</div>
					<div class="col-md-1">
						100 Tk
					</div>
						<div class="col-md-2">
							<span>1200Tk</span>
							<br>
							<div class="delete">
								<button class="btn btn-sm btn-danger">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
							</button>
							</div>
						</div>
				</div>
			</div>
			<div class="product-here">
				<div class="row">
					<div class="col-md-2">
						<img src="image/t-shirt.jpg" class="img-thumbnail" alt="">
					</div>
					<div class="col-md-2">
						<div class="product-name">
							<p>Sleveless T-shirts</p>
						</div>
						<div class="rating">
							<span class="full"></span>
							<span class="full"></span>
							<span class="full"></span>
							<span class="half"></span>
							<span class="empty"></span>
						</div>
						<div class="product-description">
							<p><strong>Color:</strong>Blue</p>
							<p><strong>Size:</strong>
							<span class="btn">L</span>
						    </p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="qty">
						<input type="number" class="qty-num" min="1"
						max="" value="1">
						</div>
					</div>
					<div class="col-md-2">
						<p>12,000</p>
					</div>
					<div class="col-md-1">
						100 Tk
					</div>
						<div class="col-md-2">
							<span>1200Tk</span>
							<br>
							<div class="delete">
								<button class="btn btn-sm btn-danger">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
							</button>
							</div>
						</div>
				</div>
			</div>	
			<div class="product-here">
				<div class="row">
					<div class="col-md-2">
						<img src="image/t-shirt.jpg" class="img-thumbnail" alt="">
					</div>
					<div class="col-md-2">
						<div class="product-name">
							<p>Sleveless T-shirts</p>
						</div>
						<div class="rating">
							<span class="full"></span>
							<span class="full"></span>
							<span class="full"></span>
							<span class="half"></span>
							<span class="empty"></span>
						</div>
						<div class="product-description">
							<p><strong>Color:</strong>Blue</p>
							<p><strong>Size:</strong>
							<span class="btn">L</span>
						    </p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="qty">
						<input type="number" class="qty-num" min="1"
						max="" value="1">
						</div>
					</div>
					<div class="col-md-2">
						<p>12,000</p>
					</div>
					<div class="col-md-1">
						100 Tk
					</div>
						<div class="col-md-2">
							<span>1200Tk</span>
							<br>
							<div class="delete">
								<button class="btn btn-sm btn-danger">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
							</button>
							</div>
						</div>
				</div>
			</div>	
			</div>
		</div>
		<!-- product-end here -->
		<div class="cart-check">
			<div class="container">
				<div class="row">
					<div class="col-md-4 offset-md-8">
						<a href="login.php">
						<button class="btn btn-primary">
							Checkout
						</button>							
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("footer.php") ?>

</body>
