<body>
	<?php include("header.php") ?>
	<div class="body">
		<div class="product-essential">
			<div class="container">
				<div class="row">
					<div class="zoom col-md-4">
				 <img id="img_01"  src="image/zoom.jpg" class="img-fluid" data-zoom-image="image/zoom.jpg"/>
						<div id="gal1">							
							<a href="#" data-image="image/zoom.jpg" data-zoom-image="image/zoom.jpg">
								<img id="img_01" src="thumb/small.jpg" /></a>
							<a href="#" data-image="image/zoom1.jpg" data-zoom-image="image/zoom1.jpg">
								<img id="img_01" src="thumb/small1.jpg" /></a>
							<a href="#" data-image="image/zoom2.jpg" data-zoom-image="image/zoom2.jpg">
							<img id="img_01" src="thumb/small2.jpg" /></a>
						</div>
					</div>
					<div class="col-md-8">
						<div class="product-description">
							<h1>This Name Of Product</h1>
							<div class="product-code">
								<h6>Product Code:#1233</h6>
							</div>
							<div class="product-rating">
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
								<a href="review.php"><small>Be the first to review this product</small>
								</a>
							</div>
							<!-- rating end -->
							<div class="details-price">
								<div class="old-price">
									<span>720 Tk</span>
								</div>
								<div class="new-price">
									<strong>Price:</strong>&nbsp;
									<span>720 Tk</span>
								</div>
								<div class="dp-price">
									<strong>DP</strong>&nbsp;
									<span>70 Tk</span>
								</div>
								<div class="vendor">
									<strong>Vendor</strong>&nbsp;
									<span>Rk Group</span>
								</div>
							</div>
							<div class="details">
								<p> Great selection of fashion and fine imported jewelry. Designer jewelry pieces to complement any outfit. We offer latest design, outstanding quality and value. So, choose from numerous of sparkling styles. </p>
							</div>
							<div class="size">
								<strong>Size</strong><br>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
									<label class="form-check-label" for="exampleRadios1">
										M
									</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
									<label class="form-check-label" for="exampleRadios2">
										L
									</label>
								</div>
								<div class="form-check form-check-inline disabled">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
									<label class="form-check-label" for="exampleRadios3">
										XL
									</label>
								</div>
								<div class="form-check form-check-inline ">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" >
									<label class="form-check-label" for="exampleRadios4">
										XXL
									</label>
								</div>
							</div>
							<div class="size-details">
								<strong>Size Details</strong><br>
								<span><span class="btn">M</span>
 									Length-28 Chest-19</span>
 									<span><span class="btn">L</span>
 									Length-28 Chest-19</span>
 									<span><span class="btn">XL</span>
 									Length-28 Chest-19</span>
 									<span><span class="btn">XXL</span>
 									Length-28 Chest-19</span>
							</div>
							<div class="qty-num">
								<strong>QTY</strong><input type="number" min="1" max="" value="1">
							</div>
							<div class="coupon">
								<strong>Coupon</strong><br>
								<input type="text">
							</div>
						</div>
						<div class="details-buy">
							<a href="cart.php">
							<button>
							<i class="fa fa-shopping-basket" aria-hidden="true"></i>
							&nbsp;Buy Now</button>								
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="topic">
			<div class="container">
				<div class="row">
					<div class="col-md-3 offset-md-4">
						<div class="">
							<img src="image/shield.png" alt="">
						</div>
						<div class="protect-text">
							<h5>100% Buyer Protection</h5>
							<small>7 Days Replacement</small>
						</div>
					</div>
					<div class="col-md-2">
						<div class="deliver">
							<img src="image/deliver.png" class="" alt="">
						</div>
						<div class="protect-text">
							<h5>Delivery Time</h5>
							<small>Usualy in 1-5 business days</small>
						</div>
					</div>
					<div class="col-md-3">
						<div class="call">
							<img src="image/call.png" alt="">
						</div>
						<div class="protect-text">
							<h5>Order By Call</h5>
							<small>01675121343</small>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="share-payment">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<h6>Share</h6>
						<a href="">
						<i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i></a>
						<a href="">
						<i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i></a>
						<a href="">
							<i class="fa fa-2x fa-pinterest-square" aria-hidden="true"></i>
						</a>
						<a href="">
						<i class="fa fa-2x fa-google-plus-square" aria-hidden="true"></i></a>
					</div>
					<div class="col-md-6 offset-md-2">
						<h6><strong>Payments</strong></h6>
						<img src="image/payment-icons.png" class="img-fluid" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="sale">
			<div class="container-fluid">
				<div class="row no-gutters">
					<div class="col-md-12">
						<img src="image/sale.png" class="container-fluid" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="product-option">
			<div class="container">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Feature</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Purchase & Delivery</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Replace Poilcy</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">Review</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="tab-details">
							<li>Brand: Jaims.</li>
							<li>Brand: Jaims.</li>
							<li>Brand: Jaims.</li>
						</div>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="tab-details">
							<div class="tab-space">
								<strong>Purchase Step</strong>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
							</div>
							<div class="tab-space">
								<strong>Purchase Step</strong>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="tab-details">
							<div class="tab-space">
								<strong>Purchase Step</strong>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
							</div>
							<div class="tab-space">
								<strong>Purchase Step</strong>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
								<li>Select number of product you want to buy.</li>
							</div>
						</div>
					</div>	
					<div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
						<div class="tab-details">
							<div class="tab-space">
								<div class="avatar">	
								<img src="image/avatar.png" alt="">
								</div>
							<div class="review-rating">
								<span>
									<i class="fa fa-star" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-half-o" aria-hidden="true">
									</i>
								</span>
								<span>
									<i class="fa fa-star-o" aria-hidden="true">
									</i>
								</span>
							</div>
								<div class="comment">
									<p>Select number of product you is good</p>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	//initiate the plugin and pass the id of the div containing gallery images
$("#img_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active',responsive:true, imageCrossfade: true}); 

//pass the images to Fancybox
$("img_01").bind("click", function(e) {  
  var ez =   $('img_01').data('elevateZoom');	
	$.fancybox(ez.getGalleryList());
  return false;
});
	</script>
	<?php include("footer.php") ?>
</body>