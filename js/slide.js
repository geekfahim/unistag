$(".slider").slick({
  infinite:true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,           
  autoplaySpeed:1000,
  pauseOnHover:true,
  arrows:false,
  dots: true,
  responsive: [{

      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        infinite: true
      }

    }, {

      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        dots: true
      }

    }, {

      breakpoint: 300,
      settings: "unslick" // destroys slick

    }]
});
