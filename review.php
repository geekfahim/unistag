<body>
	<?php include("header.php") ?>
	<div class="review-main">
		<div class="container">			
		<div class="col-md-8 offset-md-2">
<div class="review-form">	
<div class="review-head">
	<h1>Write Your Review</h1>
</div>
		<form>
  <div class="form-group">
    <label for="inputAddress">Review Title</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Review Text</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>
  <div class="form-group">
<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
									<label class="form-check-label" for="exampleRadios1">
									 Below Average
									</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
									<label class="form-check-label" for="exampleRadios2">
										Average
									</label>
								</div>
								<div class="form-check form-check-inline ">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" >
									<label class="form-check-label" for="exampleRadios3">
										Good
									</label>
								</div>
									<div class="form-check form-check-inline ">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4" value="option4" >
									<label class="form-check-label" for="exampleRadios4">
									Above Good
									</label>
								</div>
								<div class="form-check form-check-inline ">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios5" value="option5" >
									<label class="form-check-label" for="exampleRadios5">
										Excellent
									</label>
								</div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>			
	</div>
	</div>			
			</div>
		</div>
	</div>
	<?php include("footer.php") ?>
</body>